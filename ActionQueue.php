<?php namespace Decoupled\Core\Action;

class ActionQueue{

	const PROVIDER = 'action.queue';

	protected $actions = [];

	/**
	 * alias for ::add method
	 *
	 * @param      array   $actions  The actions
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */

	public function __invoke( $actions )
	{
		if( !is_array($actions) ) $actions = func_get_args();

		foreach( $actions as $action )
		{
			$this->add( $action );
		}

		return $this;
	}

	/**
	 * push an action to the queue
	 *
	 * @param      Decoupled\Core\Action\ActionInterface  $action  The action
	 *
	 * @return     Decoupled\Core\Action\ActionQueue  ( self )
	 */

	public function add( ActionInterface $action )
	{
		$this->actions[] = $action;

		return $this;
	}

	/**
	 * get list of queued actions
	 *
	 * @return     array  actions
	 */

	public function all()
	{
		return $this->actions;
	}

	/**
	 * maps callback to each action inside queue
	 *
	 * @param      callable  $callback  The callback
	 *
	 * @return     Decoupled\Core\Action\ActionQueue  ( self )
	 */

	public function map( $callback )
	{
		array_map( $callback, $this->all() );

		return $this;
	}

}