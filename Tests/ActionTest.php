<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Action\Action;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\Tests\BindObject;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Action\ActionQueue;

class ActionTest extends TestCase
{
    public function __construct()
    {
        $this->invoker = new ActionInvoker();

        $this->bindObject = new BindObject();

        $this->factory = new ActionFactory();

        $this->queue = new ActionQueue();
    }

    public function testCanBindClass()
    {
        $unit = $this;

        $action = new Action(function( $param ) use($unit){

            $unit->assertEquals( $param, 1 );

            return $this->getValue();
        });

        $bind = $this->bindObject;

        $action->bind( $bind );

        $this->assertEquals( 
            $bind->getValue(), 
            $this->invoker->invoke( $action, ['param' => 1] )
        );

        return $action;
    }

    /**
    * @depends testCanBindClass
    **/

    public function testCanAccessPrivateMethod( $action )
    {
        $bind = new BindObject();

        $action->setCallable(function(){

            return $this->getPrivateValue();
        });

        $bind->bind( $action );

        $this->assertTrue( $this->invoker->invoke($action, ['param' => 1]) );
    }

    /**
    * @depends testCanBindClass
    **/

    public function testCanInvokeinternally( $action )
    {
        $action->setInvoker( $this->invoker );

        $this->assertEquals( $action(['param'=>1]), $this->bindObject->getValue() );
    }

    public function testCanCreateInvokeableActionWithFactory()
    {
        $action = $this->factory->make(function(){

            return;
        });

        $this->assertTrue( $action instanceof Decoupled\Core\Action\Action );

        $action = $this
            ->factory
            ->setInvoker( $this->invoker )
            ->make(function(){
            
                return 1;
        });

        $this->assertEquals( $action(), 1 );
    }

    public function testCanPushToQueue()
    {
        $queue = $this->queue;

        $this->factory->setInvoker( $this->invoker );

        $queue->add($this->factory->make(function(){
            return 1;
        }));

        $queue->add($this->factory->make(function(){
            return 1;
        }));

        $this->assertEquals( count($queue->all()), 2 );

        $amt = 0;

        foreach($queue->all() as $action)
        {
            $amt += $action();
        }

        $this->assertEquals( 2, $amt );
    }
}