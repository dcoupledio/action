<?php namespace Decoupled\Core\Action\Tests;

use Decoupled\Core\Action\ActionInvokerInterface;
use Decoupled\Core\Action\Action;

class ActionInvoker implements ActionInvokerInterface{
    
    public function invoke( Action $action, array $params = [] )
    {
        $callback = $action->getCallable();

        $deps = array_merge( $action->getDeps(), array_values($params) );

        return call_user_func_array( $callback, $deps );
    }

}