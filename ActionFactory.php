<?php namespace Decoupled\Core\Action;

use Decoupled\Core\Action\ActionConverter;

class ActionFactory implements ActionFactoryInterface{


    /**
     * Service that invokes action internally if ::__invoke method
     * is called
     *
     * @var Decoupled\Core\Action\ActionInvokerInterface 
     */    

    protected $invoker;

    /**
     * alias for ActionFactory::make method
     *
     * @param      mixed  $action  The action callback
     *
     * @return     Decoupled\Core\Action\Action  action instance
     */

    public function __invoke( $action )
    {
        return $this->make( $action );
    }

    /**
     * create new action instance
     *
     * @param      mixed  $action  The action callback
     *
     * @return     Decoupled\Core\Action\Action  action instance
     */

	public function make( $action )
	{
		$action = new Action($action);

        if( $invoker = $this->getInvoker() )
        {
            $action->setInvoker( $invoker );
        }

        return $action;
	}

    /**
     * Sets the invoker object passed to action instance
     *
     * @param      Decoupled\Core\Action\ActionInvokerInterface  $invoker  The invoker
     *
     * @return     Decoupled\Core\Action\ActionFactory ( self )
     */

    public function setInvoker( ActionInvokerInterface $invoker )
    {
        $this->invoker = $invoker;

        return $this;
    }

    /**
     * Gets the invoker passed to action instance
     *
     * @return     Decoupled\Core\Action\ActionInvokerInterface  The invoker.
     */

    public function getInvoker()
    {
        return $this->invoker;
    }

}