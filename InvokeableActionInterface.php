<?php namespace Decoupled\Core\Action;

interface InvokeableActionInterface{

    /**
     * Sets the action invoker object that will invoke the Action object
     * internally, when __invoke method is called
     *
     * @param      Decoupled\Core\Action\ActionInvokerInterface  $invoker  The invoker
     *
     * @return     Decoupled\Core\Action\Action  ( self )
     */

    public function setInvoker( ActionInvokerInterface $invoker );

    /**
     * Gets the action invoker class.
     *
     * @return     Decoupled\Core\Action\ActionInvokerInterface  The invoker.
     */

    public function getInvoker();

    /**
     * invokes callable action 
     *
     * @param      array   $params  The parameters
     *
     * @return     mixed  ( result of action callback )
     */

    public function __invoke( array $params = [] );
}