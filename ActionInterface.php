<?php namespace Decoupled\Core\Action;

interface ActionInterface{

    /**
     * Gets the string dependency names for current action
     *
     * @return     array  The dependency names.
     */

	public function getDeps();

    /**
     * Gets the callable version of the action 
     *
     * @return     callable  The callable action.
     */

	public function getCallable();


    /**
     * bind the context of an object to the action.
     * *only works for closures
     *
     * @param      object  $object  The object context
     *
     * @return     Decoupled\Core\Action\Action  ( self )
     */

    public function bind( $object );
}