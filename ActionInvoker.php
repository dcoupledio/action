<?php namespace Decoupled\Core\Action;

use Decoupled\Core\Action\Action;
use Decoupled\Core\Action\ActionInvokerInterface;

class ActionInvoker implements ActionInvokerInterface{

    /**
     * invokes action, resolving deps from the $params argument 
     *
     * @param      Decoupled\Core\Action\ActionInterface    $action  The action
     * @param      array              $params  The parameters
     *
     * @throws     \RuntimeException  fails if dependency is not found in $param array
     *
     * @return     mixed             result of invoked action
     */

    public function invoke( ActionInterface $action, array $params = [] )
    {
        //this invoker ignores deps, because it is unable to resolve them
        //$params = array_merge($action->getDeps(), array_value($params));
        $deps = $action->getDeps();

        $args = [];

        //Replace the string Value Deps with actual deps
        foreach( $deps as $dep )
        {
            if( !isset($params[$dep]) ) 
            {
                throw new \RuntimeException(sprintf("Required Parameter %s is not defined", $dep));
            }

            $args[] = $params[$dep];

            //remove dep from array, because we're going to append any unfound 
            //parameters to the end of the argument array
            unset($params[$dep]);
        }

        //merge remaining params at the end of arg array
        $args = array_merge( $args, array_values($params) );

        $callable = $action->getCallable();

        return call_user_func_array( $callable, $args );
    }
}