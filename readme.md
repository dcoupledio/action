# Action Component

dcoupled Action component fires callbacks injecting dependencies 

## Install

```sh
composer require dcoupled/action v1.0.0
```

## Usage

```php

use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Action\ActionFactory;

$factory = new ActionFactory();

$factory->setInvoker( new ActionInvoker() );

$action = $factory->make(function( FooType $foo ){
    
    /* ... do something ... */
});

$action([ 'foo' => new FooType() ]);

```
## Callback Types

dcoupled Action component accepts any callable callback or string controller action with `controller@action` syntax. 

```php

// Callable Action

$closure = $factory->make(function(){
  /* ... */  
});

$func   = $factory->make('func_name');

$array  = $factory->make( [ new FooObject, 'barMethod' ] );

// Controller Action

$ctrlAction = $factory->make('ctrl@action');

```

## Binding Objects to closures

It's possible to bind object context to actions using the `::bind` method. *Note: If
the internal callback is not a Closure object, bind method will be ignored. 

```php

$action->bind( new FooObject() );

```



