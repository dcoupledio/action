<?php namespace Decoupled\Core\Action;

use Decoupled\Core\Action\ActionConverter;

class Action implements ActionInterface, InvokeableActionInterface{

	/**
	 *  action callback
	 * 
	 * @string mixed 
	 */

	protected $action;

	/**
	 * list of string dependency names
	 *
	 * @var array
	 */

	protected $deps;

	/**
	 * Service that invokes action internally if ::__invoke method
	 * is called
	 *
	 * @var Decoupled\Core\Action\ActionInvokerInterface 
	 */

	protected $invoker;

	/**
	 * @param      mixed  $action  The action
	 */

	public function __construct( $action )
	{
		$this->set( $action );
	}

	/**
	 * invokes callable action 
	 *
	 * @param      array   $params  The parameters
	 *
	 * @return     mixed  ( result of action callback )
	 */

	public function __invoke( array $params = [] )
	{
		return $this->getInvoker()->invoke( $this, $params );
	}

	/**
	 * Gets the string dependency names for current action
	 *
	 * @return     array  The dependency names.
	 */

	public function getDeps()
	{
		return @$this->action[1] ?: [];
	}

	/**
	 * Gets the callable version of the action 
	 *
	 * @return     callable  The callable action.
	 */

	public function getCallable()
	{
		return $this->action[0];
	}

	/**
	 * Sets the callable version of the action.
	 *
	 * @param      callable  $action  The action
	 *
	 * @return     Decoupled\Core\Action\Action  ( self )
	 */

	public function setCallable( callable $action )
	{
		$this->action[0] = $action;

		return $this;
	}

	/**
	 * bind the context of an object to the action.
	 * *only works for closures
	 *
	 * @param      object  $object  The object context
	 *
	 * @return     Decoupled\Core\Action\Action  ( self )
	 */

	public function bind( $object )
	{
		$action = $this->getCallable();

		if( $action instanceof \Closure )
		{
			$action = $action->bindTo( $object, $object );

			$this->setCallable( $action );
		}

		return $this;
	}

	/**
	 * Sets the action invoker object that will invoke the Action object
	 * internally, when __invoke method is called
	 *
	 * @param      Decoupled\Core\Action\ActionInvokerInterface  $invoker  The invoker
	 *
	 * @return     Decoupled\Core\Action\Action  ( self )
	 */

	public function setInvoker( ActionInvokerInterface $invoker )
	{
		$this->invoker = $invoker;

		return $this;
	}

	/**
	 * Gets the action invoker class.
	 *
	 * @return     Decoupled\Core\Action\ActionInvokerInterface  The invoker.
	 */

	public function getInvoker()
	{
		return $this->invoker;
	}

	/**
	 * sets the callback for the action
	 *
	 * @param      mixed  $action  The action
	 * 
	 * @return     Decoupled\Core\Action\Action  ( self )
	 */

	protected function set( $action )
	{
		$this->action = ActionConverter::parse( $action );

		return $this;
	}

}