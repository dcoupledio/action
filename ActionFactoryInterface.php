<?php namespace Decoupled\Core\Action;

interface ActionFactoryInterface{

    /**
     * Creates Action instance
     *
     * @param      mixed  $action  The action
     * 
     * @return     Decoupled\Core\Action\ActionInterface The Action
     */

	public function make( $action );

}