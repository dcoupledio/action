<?php namespace Decoupled\Core\Action;

interface ActionInvokerInterface{

   /**
     * invokes action, attempting to resolving deps 
     * 
     * @param      array              $params  The parameters
     *
     * @return     mixed             result of invoked action
     */

	public function invoke( ActionInterface $action, array $params = [] );

}